/**
 * camera service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService(
  'api::camera.camera',
  ({ strapi }) => ({
    async createCamerasFromPing(payload) {
      strapi.log.info('createCamerasFromPing');
      console.log('serv 1');

      const arr = [1, 2, 3, 4];

      const first = arr[0];
      const second = arr[1];

      console.log(first);
      console.log(second);

      return Promise.all(
        payload.connectivity.map(async (item) => {
          const data = {
            ipAddress: item.ipAddress,
            storeCode: payload.storeCode,
            lastContact: item.lastContact,
            status: item.status.toLowerCase() === 'connected',
            additionalAttributes: item.additionalAttributes,
          };

          await strapi.db.query('api::camera.camera').create({
            data,
          });
        })
      );
    },

    async findOneCamera(ip) {
      console.log('hello');

      console.log(ip);
      return strapi.db.query('api::camera.camera').findOne({
        where: { ipAddress: ip },
      });
    },
  })
);
