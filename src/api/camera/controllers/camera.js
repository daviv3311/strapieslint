// noinspection JSUnresolvedFunction

'use strict';

/**
 * camera controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  'api::camera.camera',
  ({ strapi }) => ({
    async createCamera(ctx) {
      const cameraDto = ctx.request.body;
      const ip = '192.168.8.38';
      try {
        console.log('Conn 1');
        await strapi
          .service('api::camera.camera')
          .createCamerasFromPing(cameraDto);

        console.log('Conn 2');
        // strapi
        //   .service('api::camera.camera')
        //   .findOneCamera(ip);

        // if (res == null) {
        //   console.log('res', res)
        // }
      } catch (error) {
        console.log('come here');
        strapi.log.error(error);
      }

      console.log('con 3');

      ctx.response.body = 'OK';

      return ctx.response.body;
    },
  })
);
